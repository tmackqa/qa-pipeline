import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/CAP')

WebUI.setText(findTestObject('Object Repository/Page_Security Authorization Certifi/input_First Name_wffm79e626365'), 'PiplineFN')

WebUI.setText(findTestObject('Object Repository/Page_Security Authorization Certifi/input_Last Name_wffm79e6263659'), 'PipelineLN')

WebUI.setText(findTestObject('Object Repository/Page_Security Authorization Certifi/input_Company_wffm79e6263659e0'), 'PipelineComp')

WebUI.setText(findTestObject('Object Repository/Page_Security Authorization Certifi/input_Email_wffm79e6263659e040'), 'pipline@mailinator.com')

WebUI.setText(findTestObject('Object Repository/Page_Security Authorization Certifi/input_Phone Number_wffm79e6263'), '8137321234567')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Security Authorization Certifi/select_CountryAfghanistanAland'), 
    'Afghanistan', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Security Authorization Certifi/select_Select Training OptionO'), 
    'Official Group Training For Your Team', true)

WebUI.click(findTestObject('Object Repository/Page_Security Authorization Certifi/input_spageurl_btn  btn-defaul'))

WebUI.closeBrowser()

